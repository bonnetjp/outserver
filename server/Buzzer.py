import RPi.GPIO as GPIO
import time

class Buzzer:

    def __init__(self):
        self.pin = 18
        GPIO.setup(self.pin, GPIO.OUT)  # Set LedPin's mode is output
        GPIO.output(self.pin, GPIO.HIGH)

    def buzz(self, t):
        GPIO.output(self.pin, GPIO.LOW)  # led on
        time.sleep(t/ 1000.)
        GPIO.output(self.pin, GPIO.HIGH)

    def off(self):
        print 'turning off buzzer'
        GPIO.output(self.pin, GPIO.HIGH)
