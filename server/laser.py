import RPi.GPIO as GPIO
import time

class Laser:

    def __init__(self):
        self.pin = 16
        GPIO.setup(self.pin, GPIO.OUT)  # Set LedPin's mode is output
        GPIO.output(self.pin, GPIO.HIGH)

    def shoot(self, t):
        GPIO.output(self.pin, GPIO.LOW)  # led on
        time.sleep(t/ 1000.)
        GPIO.output(self.pin, GPIO.HIGH)

    def off(self):
        print 'turning off laser'
        GPIO.output(self.pin, GPIO.HIGH)


