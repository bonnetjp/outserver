#!/usr/bin/env python
"""process image using Azure Face API
Usage:
    python process_img.py "imageURL"
"""
import argparse
import cognitive_face as CF


def detect_img(img_url):
    """Detect the image face
    """
    cogkey = '7242691590654fd088c381cf0138faa3'
    CF.Key.set(cogkey)

    result = CF.face.detect(img_url)
    if result:
        result = result[0]['faceId']
        return result
    else:
        return 0


def ident_img(faceid):
    """Identify if the face is friendly
    """
    result = CF.face.identify([faceid], 'friendly', 1, 0.75)
    if result[0]['candidates'] != []:
        return 1
    else:
        return 0


def process(img_name):
    """The main function
    """


    img_url = 'https://techsummitteam3storage.blob.core.windows.net/images/' + img_name
    # do stuff
    friend_status = 'unknown'
    faceid = detect_img(img_url)
    if faceid != 0:
        face_status = ident_img(faceid)
        if face_status == 1:
            friend_status = 'friend'
        else:
            friend_status = 'hostile'
    else:
        friend_status = 'unknown'
    
    return friend_status


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('img_name', help='the image name')
    args = parser.parse_args()
    process(args.img_name)
