import RPi.GPIO as GPIO
import time

class Ultrasonic:

    def __init__(self):
        self.echo = 36
        self.trigger = 37
        GPIO.setup(self.trigger, GPIO.OUT)
        GPIO.setup(self.echo, GPIO.IN)

    def distance(self):
        GPIO.output(self.trigger, 0)
        time.sleep(0.000002)

        GPIO.output(self.trigger, 1)
        time.sleep(0.00001)
        GPIO.output(self.trigger, 0)

        while GPIO.input(self.echo) == 0:
            a = 0
        time1 = time.time()
        while GPIO.input(self.echo) == 1:
            a = 1
        time2 = time.time()

        during = time2 - time1
        return during * 340 / 2 * 100

