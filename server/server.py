from flask import Flask
from laser import Laser
from Buzzer import Buzzer
from Ultrasonic import Ultrasonic
import headshot
import process_img
import RPi.GPIO as GPIO
import logging
import time
from flask import jsonify
from flask import json
from flask_cors import CORS, cross_origin


app = Flask(__name__)
CORS(app)


global laser
global buzzer
global ultrasonic


@app.route('/laser/<int:time>')
def shoot_laser(time):
    laser.shoot(time)
    return 'shoot laser ' + str(time) + 'ms'

@app.route('/buzz/<int:time>')
def buzz(time):
    buzzer.buzz(time)
    return 'buzz ' + str(time) + 'ms'

@app.route('/distance')
def distance():
    return str(ultrasonic.distance())

@app.route('/status')
def status():
    url = 'new_subject_' + str(int(time.time())) + '.jpg'
    headshot.shot(url)
    res = dict()
    res['status'] = process_img.process(url)
    response = app.response_class(
        response=json.dumps(res),
        status=200,
        mimetype='application/json'
    )
    return response



if __name__ == "__main__":
    try:
        GPIO.setmode(GPIO.BOARD)
        laser = Laser()
        buzzer = Buzzer()
        ultrasonic = Ultrasonic()
        logging.basicConfig(filename='error.log', level=logging.DEBUG)
        app.run(host="0.0.0.0")

        laser.off()
        buzzer.off()
        GPIO.cleanup()
    except KeyboardInterrupt:
        laser.off()
        buzzer.off()
        GPIO.cleanup()

